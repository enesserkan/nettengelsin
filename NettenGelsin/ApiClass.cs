﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using Newtonsoft.Json;
using System.IO;
using Microsoft.VisualBasic;
using System.Timers;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;
using MySql.Data.MySqlClient;


namespace NettenGelsin
{
    public static class Thread_Apiİşlemleri
    {
        public static int bekleme = 5;
        public static int tekrar = 10;
        public static int tekrarÇarpan = 3; //request aralığı-->bekleme eğer hata verirse tekrarÇarpan ile çarpılıp tekrar denenecek. kaç tekrar olacağını tekrar belirliyor.
        static Thread t_kontrol;
        static List<Thread> İşlem = new List<Thread>();
        static void kontrol()
        {
            int i;
            while (true)
            {
                i = 0;
                while (i < İşlem.Count)
                {
                    if (!İşlem[i].IsAlive) lock (İşlem) { İşlem.RemoveAt(i); }
                    else i++;
                }
                Thread.Sleep(33);
            }
        }
        public static void Başla()
        {
            t_kontrol = new Thread(kontrol);
            t_kontrol.IsBackground = true;
            t_kontrol.Start();
        }
        public static void işEkle(Thread eklenecek_iş)
        {
            İşlem.Add(eklenecek_iş);
        }
        public static void işlemler_Suspend()
        {
            foreach (Thread iş in İşlem) if (iş.IsAlive) if (iş.ThreadState == ThreadState.Running) iş.Suspend();
        }
        public static void işlemler_Resume()
        {
            foreach (Thread iş in İşlem) if (iş.IsAlive) if (iş.ThreadState == ThreadState.Suspended) iş.Resume();
        }
    }

    public static class TokenKeySınıfı
    {
        public static string access_token = "";
        public static int expires_in = 0;
        public static string token_type = "";
        public static string scope = "";
        public static string refresh_token = "";
        public static DateTime yenilenme_zamani;
        public static DateTime olusturma_zamani;
        //public Timer saat = new Timer(1000);
        public static Thread kontrol;
        public static ToolStripStatusLabel bilgilendirme;
        public static void Başla()
        {
            yenilenme_zamani = olusturma_zamani = DateTime.Now;
            dosyadanOku();
            kontrol = new Thread(new ThreadStart(kontrol_));
            kontrol.Start();
        }
        private static void kontrol_()
        {
            if (bilgilendirme == null) bilgilendirme = new ToolStripStatusLabel();
            while (true)
            {
                if (aktif()) bilgilendirme.Text = "Durum: Aktif    Token key " + geçerlilikSüresi + " daha geçerli.";
                else
                {

                    if (refreshable())
                    {
                        bilgilendirme.Text = "Durum: Güncelleniyor    Token key kullanım süresi bitti. Tazeleniyor";
                        Thread_Apiİşlemleri.işlemler_Suspend();
                        Thread.Sleep(15000);
                        bool sonuç;
                        int bekleme = Thread_Apiİşlemleri.bekleme;
                        int tekrar = 0;
                        do
                        {
                            tekrar++;
                            sonuç = RefreshToken();
                            if (!sonuç)
                            {
                                Thread.Sleep(bekleme);
                                bekleme *= Thread_Apiİşlemleri.tekrarÇarpan;
                                if (tekrar == Thread_Apiİşlemleri.tekrar)
                                {
                                    MessageBox.Show("Token Key yenilenemedi.. Programın çalışması durdu..", "Hata !", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    break;
                                }
                            }
                        } while (!sonuç);
                        if (sonuç) Thread_Apiİşlemleri.işlemler_Resume();
                    }
                    else
                    {

                        bilgilendirme.Text = "Durum: Pasif    Token key kullanım süresi bitti. Tazeleme işlemi için kullanılan anahtarında kullanım süresi bitti. Yeniden yetki verilmesi gerekli.";
                        GetToken();
                    }
                }
                Thread.Sleep(500);
            }
        }
        public static bool dosyadanOku()
        {
            FileInfo f = new FileInfo("managementFiles/token.txt");
            if (f.Exists)
            {
                FileStream file = new FileStream(f.FullName, FileMode.Open);
                bool sonuc = false;
                try
                {
                    StreamReader r = new StreamReader(file, Encoding.UTF8);
                    string x = r.ReadLine().Trim();
                    olusturma_zamani = DateTime.Parse(x);
                    access_token = r.ReadLine().Trim();
                    refresh_token = r.ReadLine().Trim();
                    expires_in = int.Parse(r.ReadLine().Trim());
                    token_type = r.ReadLine().Trim();
                    scope = r.ReadLine().Trim();
                    yenilenme_zamani = DateTime.Parse(r.ReadLine().Trim());
                    sonuc = true;
                    r.Close();
                }
                finally
                {
                    file.Close();
                }

                return sonuc;
            }
            else return false;
        }
        public static bool dosyayaYaz()
        {
            FileStream file = new FileStream("managementFiles/token.txt", FileMode.Create);
            StreamWriter w = new StreamWriter(file, Encoding.UTF8);
            bool sonuc = false;
            try
            {
                w.WriteLine(olusturma_zamani.ToString());
                w.WriteLine(access_token);
                w.WriteLine(refresh_token);
                w.WriteLine(expires_in.ToString());
                w.WriteLine(token_type);
                w.WriteLine(scope);
                w.WriteLine(yenilenme_zamani.ToString());
                sonuc = true;
                w.Close();
            }
            finally
            {
                file.Close();
            }
            return sonuc;
        }
        public static bool refreshable()
        {
            if (access_token == "") return false;
            DateTime son_gecerlilik_tarihi = olusturma_zamani.AddDays(60); // 2 ay refresh yapılabiliyor onun için
            return DateTime.Now <= son_gecerlilik_tarihi;
        }
        public static string geçerlilikSüresi
        {
            get
            {
                double fark = yenilenme_zamani.AddSeconds((double)expires_in).Subtract(DateTime.Now).TotalSeconds;
                int saat = (int)(fark / 3600.0);
                int dakika = (int)((fark - (saat * 3600)) / 60.0);
                int saniye = (int)(fark - (saat * 3600) - (dakika * 60));
                return (saat > 0 ? saat.ToString() + " saat " : "") + (dakika > 0 ? dakika.ToString() + " dakika " : "") + (saniye > 0 ? saniye.ToString() + " saniye" : "");
            }
        }
        public static bool aktif()
        {
            if (access_token == "") return false;
            double fark = DateTime.Now.Subtract(yenilenme_zamani).TotalSeconds;
            return fark < (21600 - 10); // 21600 sn yani 6 saat eder.
        }
        public static bool GetToken()
        {
            string code = "http://" + Entegrasyon.siteAdresi + "/admin/user/auth?client_id=" + Entegrasyon.clientId + "&response_type=code&state=2b33fdd45jbevd6nam&redirect_uri=" + Entegrasyon.redirectUri;
            //client.Timeout = -1;
            code = Interaction.InputBox("Api işlemlerinin yapılabilmesi için Token değerine ihtiyaç var.\nAşağıda verilen adres bilgisini tarayıcının adres çubuğuna yapıştırınız. Gelen adres verisi içinden 'code=' den sonraki kod değerini yine aşağıdaki yere giriniz.(Kod değeri 30 sn. geçerlidir. !!)", "Code değeri gerekli !!!", code);
            code = code.Trim();
            if (code == "")
            {
                return false;
            }

            var client = new RestClient("http://" + Entegrasyon.siteAdresi + "/oauth/v2/token");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AlwaysMultipartFormData = true;
            request.AddParameter("grant_type", "authorization_code");
            request.AddParameter("client_id", Entegrasyon.clientId);
            request.AddParameter("client_secret", Entegrasyon.clientSecret);
            request.AddParameter("redirect_uri", Entegrasyon.redirectUri);
            request.AddParameter("code", code);
            try
            {
                IRestResponse response = client.Execute(request);
                if (response.StatusCode.ToString() == "OK")
                {
                    dynamic data = JsonConvert.DeserializeObject(response.Content);
                    access_token = data["access_token"].Value;
                    expires_in = (int)data["expires_in"].Value;
                    token_type = data["token_type"].Value;
                    scope = data["scope"].Value;
                    refresh_token = data["refresh_token"].Value;
                    yenilenme_zamani = olusturma_zamani = DateTime.Now;
                    dosyayaYaz();
                    return true;
                }
                return false;
            }
            catch (Exception hata)
            {
                System.Windows.Forms.MessageBox.Show(hata.Message);
                return false;
            }
        }
        public static bool RefreshToken()
        {
            var client = new RestClient("http://" + Entegrasyon.siteAdresi + "/oauth/v2/token");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AlwaysMultipartFormData = true;
            request.AddParameter("grant_type", "refresh_token");
            request.AddParameter("client_id", Entegrasyon.clientId);
            request.AddParameter("client_secret", Entegrasyon.clientSecret);
            request.AddParameter("refresh_token", refresh_token);
            IRestResponse response = client.Execute(request);
            if (response.StatusCode.ToString() == "OK")
            {
                dynamic data = JsonConvert.DeserializeObject(response.Content);
                access_token = data["access_token"].Value;
                expires_in = (int)data["expires_in"].Value;
                token_type = data["token_type"].Value;
                scope = data["scope"].Value;
                refresh_token = data["refresh_token"].Value;
                yenilenme_zamani = DateTime.Now;
                dosyayaYaz();
                return true;
            }
            else return false;
        }
    }

    public class SatınAlmaLimiti
    {
        public Nesne_SatınAlmaLimiti[] satınAlmaLimitleriListesi = new Nesne_SatınAlmaLimiti[0];

        public void satınAlmaLimitleriListesiniDoldur()
        {
            Array.Resize(ref satınAlmaLimitleriListesi, 0);
            int page = 1;
            IRestResponse data;
            bool devam = true;
            while (devam)
            {
                data = Entegrasyon.GET("purchase_limitations", "limit=10&page=" + page.ToString());
                page++;
                if (devam = data.IsSuccessful)
                {
                    dynamic d = JsonConvert.DeserializeObject(data.Content);
                    if (devam = (d.Count > 0))
                    {
                        foreach (var item in d)
                        {
                            Array.Resize(ref satınAlmaLimitleriListesi, satınAlmaLimitleriListesi.Length + 1);
                            satınAlmaLimitleriListesi[satınAlmaLimitleriListesi.Length - 1] = new Nesne_SatınAlmaLimiti(item);
                        }
                    }
                }
            }
        }

        public bool satınAlmaLimitiKalemiOluştur()
        {
            var client = new RestClient(Entegrasyon.Api_endPoint + "purchase_limitation_items");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            string Body = "{\n\t\"limitation\": {\"id\": 3},\n\t\"product\": {\"id\": 4106772}\n}";
            request.AddHeader("Host", Entegrasyon.Host);
            request.AddHeader("Content-Type", Entegrasyon.Content_Type);
            request.AddHeader("Content-Encoding", Entegrasyon.Content_Encoding);
            request.AddHeader("Content-Length", Entegrasyon.Content_Length(Body));
            request.AddHeader("Authorization", "Bearer " + TokenKeySınıfı.access_token);
            request.AddParameter(Entegrasyon.Content_Type, Body, ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);
            return true;
        }

        public int satınAlmaLimiti_ID_Getir(double paket_miktarı)
        {
            foreach (Nesne_SatınAlmaLimiti item in satınAlmaLimitleriListesi)
                if (item.minimumLimit == paket_miktarı) return item.id;
            return -1;
        }
    }

    public class Entegrasyon
    {
        public static string clientId = "10_12ty6lpdqgms8wsokg8oo8004kc0oc04g80444wkkw80kocooo";
        public static string clientSecret = "32i34rc58s8w0cgwgk4s8k0gckoc8g4cw8kc44g0gk0ss8s8sg";
        public static string redirectUri = "www.otobkm.com";
        public static string siteAdresi = "www.nettengelsin.com";
        public static string firmaAdı = "nettengelsin-1";
        public static string Content_Type = "application/json; charset=utf-8";
        public static string Content_Encoding = "gzip";

        public static string Api_endPoint = "http://" + firmaAdı + ".myideasoft.com/api/";
        public static string Host = firmaAdı + ".myideasoft.com";
        public static string Content_Length(string Body) { return Body.Length.ToString(); }

        public Entegrasyon()
        {
            DirectoryInfo d = new DirectoryInfo("managementFiles");
            if (!d.Exists) d.Create();
            FileInfo f = new FileInfo("managementFiles/EntegrasyonData.txt");
            if (f.Exists)
            {
                FileStream file = new FileStream(f.FullName, FileMode.Open);
                StreamReader r = new StreamReader(file, Encoding.UTF8);
                try
                {
                    string s;
                    if ((s = r.ReadLine().Trim()) != null) clientId = s;
                    if ((s = r.ReadLine().Trim()) != null) clientSecret = s;
                    if ((s = r.ReadLine().Trim()) != null) redirectUri = s;
                    if ((s = r.ReadLine().Trim()) != null) siteAdresi = s;
                    if ((s = r.ReadLine().Trim()) != null) firmaAdı = s;
                    if ((s = r.ReadLine().Trim()) != null) Content_Type = s;
                    if ((s = r.ReadLine().Trim()) != null) Content_Encoding = s;
                }
                finally
                {
                    r.Close();
                    file.Close();
                }
            }
            else
            {
                FileStream file = new FileStream(f.FullName, FileMode.CreateNew);
                StreamWriter w = new StreamWriter(file, Encoding.UTF8);
                w.WriteLine(clientId);
                w.WriteLine(clientSecret);
                w.WriteLine(redirectUri);
                w.WriteLine(siteAdresi);
                w.WriteLine(firmaAdı);
                w.WriteLine(Content_Type);
                w.WriteLine(Content_Encoding);
                w.Close();
                file.Close();

            }

            if (!TokenKeySınıfı.dosyadanOku()) TokenKeySınıfı.dosyayaYaz();
            if (!TokenKeySınıfı.aktif())
                if (TokenKeySınıfı.refreshable())
                {
                    TokenKeySınıfı.RefreshToken();
                }
                else TokenKeySınıfı.GetToken();
            TokenKeySınıfı.kontrol.Start();
        }

        public static IRestResponse POST(string istek, string body)
        {
            var client = new RestClient(Entegrasyon.Api_endPoint + istek);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Host", Entegrasyon.Host);
            request.AddHeader("Content-Type", Entegrasyon.Content_Type);
            request.AddHeader("Content-Encoding", Entegrasyon.Content_Encoding);
            request.AddHeader("Content-Length", Entegrasyon.Content_Length(body));
            request.AddHeader("Authorization", "Bearer " + TokenKeySınıfı.access_token);
            request.AddParameter("application/json; charset=utf-8", body, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response;
        }
        public static IRestResponse PUT(string istek, string id, string body)
        {
            var client = new RestClient(Entegrasyon.Api_endPoint + istek + "/" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.PUT);
            request.AddHeader("Host", Entegrasyon.Host);
            request.AddHeader("Content-Type", Entegrasyon.Content_Type);
            request.AddHeader("Content-Encoding", Entegrasyon.Content_Encoding);
            request.AddHeader("Content-Length", Entegrasyon.Content_Length(body));
            request.AddHeader("Authorization", "Bearer " + TokenKeySınıfı.access_token);
            request.AddParameter("application/json; charset=utf-8", body, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response;
        }
        public static IRestResponse GET(string istek, string parametre = "")
        {
            var client = new RestClient(Entegrasyon.Api_endPoint + istek + (parametre != "" ? "?" + parametre : ""));
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Host", Entegrasyon.Host);
            request.AddHeader("Content-Type", Entegrasyon.Content_Type);
            request.AddHeader("Authorization", "Bearer " + TokenKeySınıfı.access_token);
            IRestResponse response = client.Execute(request);
            return response;
        }
        public static IRestResponse DELETE(string istek, string id)
        {
            var client = new RestClient(Entegrasyon.Api_endPoint + istek + "/" + id);
            
            var request = new RestRequest(Method.DELETE);
            request.AddHeader("Host", Entegrasyon.Host);
            request.AddHeader("Content-Type", Entegrasyon.Content_Type);
            request.AddHeader("Authorization", "Bearer " + TokenKeySınıfı.access_token);
            IRestResponse response = client.Execute(request);
            return response;
        }

        // ------------------------- İdeasoft APİ ile İdeaSoft Sunucusunda işlem Görüyor -------------------------------------
        public static object[] ÜrünKategoriBağı(int id, string ürünID, string categoryId)
        {
            string body;
            if (id == -1) body = "{\"sortOrder\":9999,\"product\":{\"id\":" + ürünID + "},\"category\":{\"id\":" + categoryId + "}}";
            else body = "{\"id\":" + id.ToString() + ",\"sortOrder\":9999,\"product\":{\"id\":" + ürünID + "},\"category\":{\"id\":" + categoryId + "}}";
            IRestResponse data = id == -1 ? (Entegrasyon.POST("product_to_categories", body)) : (Entegrasyon.PUT("product_to_categories", id.ToString(), body));
            if (data.IsSuccessful)
            {
                dynamic d = JsonConvert.DeserializeObject(data.Content);
                if (d["id"] == null) return new object[] { false };
                else return new object[] { true, d };
            }
            else return new object[] { false };
        }
        public static object[] SatınAlmaLimitiKalemi(int id, string ürünID, string satınAlmaLimitiID)
        {
            string body;
            if (id == -1) body = "{\"limitation\":{\"id\":" + satınAlmaLimitiID + "},\"product\":{\"id\":" + ürünID + "}}";
            else body = "{\"id\":" + id.ToString() + ",\"limitation\":{\"id\":" + satınAlmaLimitiID + "},\"product\":{\"id\":" + ürünID + "}}";
            IRestResponse data = id == -1 ? (Entegrasyon.POST("purchase_limitation_items", body)) : (Entegrasyon.PUT("purchase_limitation_items", id.ToString(), body));
            if (data.IsSuccessful)
            {
                dynamic d = JsonConvert.DeserializeObject(data.Content);
                if (d["id"] == null) return new object[] { false };
                else return new object[] { true, d };
            }
            else return new object[] { false };
        }
        public static object[] ÜrünDetay(int id,string ürünID, string detail)
        {
            string body;
            if (id == -1) body = "{\"details\":\""+ detail + "\",\"product\":{\"id\":" + ürünID + "}}";
            else body = "{\"id\":" + id.ToString() + ",\"details\":\"" + detail + "\",\"product\":{\"id\":" + ürünID + "}}";
            IRestResponse data = id == -1 ? (Entegrasyon.POST("product_details", body)) : (Entegrasyon.PUT("product_details", id.ToString(), body));
            if (data.IsSuccessful)
            {
                dynamic d = JsonConvert.DeserializeObject(data.Content);
                if (d["id"] == null) return new object[] { false };
                else return new object[] { true, d };
            }
            else return new object[] { false };
        }

        public static object[] KategoriOluştur(string name, int parentId,MySqlConnection con)
        {
            string body = "{\"name\":\"" + name + "\",\"sortOrder\":" + (parentId == -1 ? IdeaSoftVeritabanı.sortOrderGetir(con).ToString() : "999") + ",\"status\":1,\"displayShowcaseContent\":0,\"showcaseContentDisplayType\": 1,\"percent\":1.0,\"parent\":" + (parentId == -1 ? "null" : ("{\"id\":" + parentId.ToString())) + "}}";
            IRestResponse data = Entegrasyon.POST("categories", body);
            if (data.IsSuccessful)
            {
                dynamic d = JsonConvert.DeserializeObject(data.Content);
                if (d["id"] == null) return new object[] { false };
                else return new object[] { true, d };
            }
            else return new object[] { false };
        }
        public static object[] MarkaOluştur(string name)
        {
            string body = "{\"name\":\"" + name + "\",\"sortOrder\":999,\"status\":1}}";
            IRestResponse data = Entegrasyon.POST("brands", body);
            if (data.IsSuccessful)
            {
                dynamic d = JsonConvert.DeserializeObject(data.Content);
                if (d["id"] == null) return new object[] { false };
                else return new object[] { true, d };
            }
            else return new object[] { false };
        }
        public static object[] SatınAlmaLimitiOluştur(string limit)
        {
            string body = "{\"name\":\"" + limit+" adet" + "\",\"minimumLimit\":"+limit+ ",\"maximumLimit\":1000,\"type\":\"product\",\"status\":true}}";
            IRestResponse data = Entegrasyon.POST("purchase_limitations", body);
            if (data.IsSuccessful)
            {
                dynamic d = JsonConvert.DeserializeObject(data.Content);
                if (d["id"] == null) return new object[] { false };
                else return new object[] { true, d };
            }
            else return new object[] { false };
        }


        public static bool ÜrünKategoriBağıSil(string ürünId,MySqlConnection con)
        {
            object[] x = IdeaSoftVeritabanı.productToCategoryIDGetir(ürünId, con);
            int id = (int)x[0];
            if (id == -1) return true;
            int bekleme = Thread_Apiİşlemleri.bekleme;
            IRestResponse data;
            while (true)
            {
                Thread.Sleep(bekleme);
                data = DELETE("product_to_categories", id.ToString());
                if (data.IsSuccessful || bekleme > 3000) break;
                bekleme *= 3;
            }
            if (data.Content == "") return true;
            else return false;
        }
        public static bool SatınAlmaLimitiKalemiSil(string ürünId,MySqlConnection con)
        {
            object[] x = IdeaSoftVeritabanı.purchase_limitation_itemsIDGetir(ürünId, con);
            int id = (int)x[0];
            if (id == -1) return true;
            int bekleme = Thread_Apiİşlemleri.bekleme;
            IRestResponse data;
            while (true)
            {
                Thread.Sleep(bekleme);
                data = DELETE("purchase_limitation_items", id.ToString());
                if (data.IsSuccessful || bekleme > 3000) break;
                bekleme *= 3;
            }
            if (data.Content == "") return true;
            else return false;
        }
        public static bool ÜrünDetaySil(string ürünId,MySqlConnection con)
        {
            object[] x = IdeaSoftVeritabanı.ürünDetailsIDGetir(ürünId, con);
            int id = (int)x[0];
            if (id == -1) return true;
            int bekleme = Thread_Apiİşlemleri.bekleme;
            IRestResponse data;
            while (true)
            {
                Thread.Sleep(bekleme);
                data = DELETE("product_details", id.ToString());
                if (data.IsSuccessful || bekleme > 3000) break;
                bekleme *= 3;
            }
            if (data.Content == "") return true;
            else return false;
        }

        // ----------------- Yerel İdeasoft veritabanını kontrol ediyor. Varsa oradan true,ID getiriyor. 
        // ----------------- Yoksa hem ideSofta hem de yerel İdeasoft veritabanına ekleme yapıp true,Id değerini getiriyor. 
        // ----------------- Başarısız ise False değerini döndürüyor.

        public static bool SatınAlmaLimitiKalemi(string ürünID, string satınAlmaLimitiID, MySqlConnection con)
        {
            object[] x = IdeaSoftVeritabanı.purchase_limitation_itemsIDGetir(ürünID, con);
            int id = (int)x[0];
            if (id != -1 && (string)x[1] == satınAlmaLimitiID) return true;

            int bekleme = Thread_Apiİşlemleri.bekleme;
            object[] sonuç;
            while (true)
            {
                Thread.Sleep(bekleme);
                sonuç = SatınAlmaLimitiKalemi(id, ürünID, satınAlmaLimitiID);
                if ((bool)sonuç[0] || bekleme > 10000) break;
                bekleme *= 3;
            }
            if ((bool)sonuç[0])
            {
                IdeaSoftVeritabanı.purchase_limitation_itemsOluştur(sonuç[1], id == -1, con);
                return true;
            }
            else return false;
        }
        public static bool ÜrünKategoriBağı(string ürünID, string categoryID, MySqlConnection con)
        {
            object[] x = IdeaSoftVeritabanı.productToCategoryIDGetir(ürünID, con);
            int id = (int)x[0];
            if (id != -1 && (string)x[1] == categoryID) return true;

            int bekleme = Thread_Apiİşlemleri.bekleme;
            object[] sonuç;
            while (true)
            {
                Thread.Sleep(bekleme);
                sonuç = ÜrünKategoriBağı(id, ürünID, categoryID);
                if ((bool)sonuç[0] || bekleme > 10000) break;
                bekleme *= 3;
            }
            if ((bool)sonuç[0])
            {
                IdeaSoftVeritabanı.productToCategoryOluştur(sonuç[1], id == -1, con);
                return true;
            }
            else return false;
        }
        public static bool ÜrünDetay(string ürünID, string detail, MySqlConnection con)
        {
            object[] x = IdeaSoftVeritabanı.ürünDetailsIDGetir(ürünID, con);
            int id = (int)x[0];
            if (id != -1 && (string)x[1] == detail) return true;

            int bekleme = Thread_Apiİşlemleri.bekleme;
            object[] sonuç;
            while (true)
            {
                Thread.Sleep(bekleme);
                sonuç = ÜrünDetay(id, ürünID, detail);
                if ((bool)sonuç[0] || bekleme > 10000) break;
                bekleme *= 3;
            }
            if ((bool)sonuç[0])
            {
                IdeaSoftVeritabanı.ürünDetailsOluştur(sonuç[1], id == -1, con);
                return true;
            }
            else return false;
        }

        public static object[] KategoriIDGetir(string category_path,MySqlConnection con)
        {
            string[] kategoriler = category_path.Split('>');
            int parent_id = -1;
            for (int i = 0; i < kategoriler.Length; i++)
            {
                int id = IdeaSoftVeritabanı.kategoryIDGetir(kategoriler[i], parent_id, con);
                if (id == -1)
                {
                    int bekleme = Thread_Apiİşlemleri.bekleme;
                    object[] sonuç;
                    while (true)
                    {
                        Thread.Sleep(bekleme);
                        sonuç = KategoriOluştur(kategoriler[i], parent_id, con);
                        if ((bool)sonuç[0] || bekleme > 10000) break;
                        bekleme *= 3;
                    }
                    if ((bool)sonuç[0]) parent_id = IdeaSoftVeritabanı.kategoriOluştur(sonuç[1], con);
                    else return new object[] { false };
                }
                else parent_id = id;
            }
            return new object[] { true,parent_id };
        }
        public static object[] MarkaIDGetir(string marka, MySqlConnection con)
        {
            int id = IdeaSoftVeritabanı.brandIdGetir(marka, con);
            if (id == -1)
            {
                int bekleme = Thread_Apiİşlemleri.bekleme;
                object[] sonuç;
                while (true)
                {
                    Thread.Sleep(bekleme);
                    sonuç = MarkaOluştur(marka);
                    if ((bool)sonuç[0] || bekleme > 10000) break;
                    bekleme *= 3;
                }
                if ((bool)sonuç[0]) id = IdeaSoftVeritabanı.brandOluştur(sonuç[1], con);
                else return new object[] { false };
            }
            return new object[] { true, id };
        }
        public static object[] SatınAlmaLimitiIDGetir(string limit, MySqlConnection con)
        {
            int id = IdeaSoftVeritabanı.satınAlmaLimitiIDGetir(limit, con);
            if (id == -1)
            {
                int bekleme = Thread_Apiİşlemleri.bekleme;
                object[] sonuç;
                while (true)
                {
                    Thread.Sleep(bekleme);
                    sonuç = SatınAlmaLimitiOluştur(limit);
                    if ((bool)sonuç[0] || bekleme > 10000) break;
                    bekleme *= 3;
                }
                if ((bool)sonuç[0]) id = IdeaSoftVeritabanı.satınAlmaLimitiOluştur(sonuç[1], con);
                else return new object[] { false };
            }
            return new object[] { true, id };
        }
        public static IRestResponse[] datas = new IRestResponse[0];
        public static bool ÜrünSil(string stokKodu, MySqlConnection con)
        {
            MySqlCommand cmd = new MySqlCommand(string.Format("select id from products where sku='{0}'",stokKodu), con);
            MySqlDataReader dr = cmd.ExecuteReader();
            string ürünID = "";
            if (dr.Read()) ürünID = dr.GetInt32(0).ToString();
            dr.Close();
            int bekleme = Thread_Apiİşlemleri.bekleme;
            log.Yaz("--------------------------------------------------------------------");
            if (ürünID != "")
            {
                IRestResponse data;
                while (true)
                {
                    log.Yaz(DateTime.Now.ToShortTimeString()+"\t"+ ürünID + " siliniyor.\t" + bekleme.ToString() + " sn. bekleniyor.");
                    Thread.Sleep(bekleme);
                    data = DELETE("products", ürünID);
                    if (data.IsSuccessful || bekleme > 10000) break;
                    bekleme *= 3;
                }
                log.Yaz(DateTime.Now.ToShortTimeString() + "\t" + ürünID + (data.Content==""?" Silindi.":" Silinemedi"));
                Array.Resize(ref datas, datas.Length + 1);
                datas[datas.Length - 1] = data;
                if (data.Content == "")
                {
                    //log.Yaz(DateTime.Now.ToShortTimeString() + "\t" + ürünID + " ürünDetay siliniyor.");
                    //x=ÜrünDetaySil(ürünID, con);
                    //log.Yaz(DateTime.Now.ToShortTimeString() + "\t" + ürünID + " ürünDetay "+(x?"silindi.":"silinemedi."));
                    //log.Yaz(DateTime.Now.ToShortTimeString() + "\t" + ürünID + " satınAlmaLimitiKalemi siliniyor.");
                    //x =SatınAlmaLimitiKalemiSil(ürünID, con);
                    //log.Yaz(DateTime.Now.ToShortTimeString() + "\t" + ürünID + " satınAlmaLimitiKalemi " + (x ? "silindi." : "silinemedi."));
                    //log.Yaz(DateTime.Now.ToShortTimeString() + "\t" + ürünID + " ürünKategoriBağı siliniyor.");
                    //x =ÜrünKategoriBağıSil(ürünID, con);
                    //log.Yaz(DateTime.Now.ToShortTimeString() + "\t" + ürünID + " ürünKategoriBağı " + (x ? "silindi." : "silinemedi."));
                    //log.Yaz(DateTime.Now.ToShortTimeString() + "\t" + ürünID + " ürünü veritabanından siliniyor.");
                    //IdeaSoftVeritabanı.ürünSil(stokKodu, con);
                    //log.Yaz(DateTime.Now.ToShortTimeString() + "\t" + ürünID + " ürünü veritabanından silindi.");
                    return true;
                }
            }
            return false;
        }

        //public static object[] ürünEkle(string sku,MySqlConnection con)
        //{

        //}
        //public static object[] ürünGüncelle(string sku, MySqlConnection con)
        //{

        //}

    }
}