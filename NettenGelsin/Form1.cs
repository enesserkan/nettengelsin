﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using MySql.Data.MySqlClient;

namespace NettenGelsin
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        bool işlem()
        {
            return true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Control.CheckForIllegalCrossThreadCalls = false;
            veritabanı.Başla();
            //veriÇekme.motoraşinBilgilendirme = label1;
            //veriÇekme.dinamikBilgilendirme = label2;
            TokenKeySınıfı.bilgilendirme = tokenBilgilendirme;
            TokenKeySınıfı.Başla();
            IdeaSoftVeritabanı.BAŞLA();
            IdeaSoftVeritabanı.brand.l = label3;
            IdeaSoftVeritabanı.currency.l = label4;
            IdeaSoftVeritabanı.price.l = label5;
            IdeaSoftVeritabanı.details.l = label6;
            IdeaSoftVeritabanı.category.l = label7;
            IdeaSoftVeritabanı.product_to_category.l = label8;
            IdeaSoftVeritabanı.product_image.l = label9;
            IdeaSoftVeritabanı.purchase_limitations.l = label10;
            IdeaSoftVeritabanı.purchase_limitation_items.l = label11;
            IdeaSoftVeritabanı.product.l = label12;
            IdeaSoftVeritabanı.tags.l = label13;
            IdeaSoftVeritabanı.product_to_tags.l = label14;

            firmalar.BAŞLA();

            //zamanlamaSınıfı z = new zamanlamaSınıfı(DateTime.Today.AddHours(23).AddMinutes(47), DateTime.Today.AddHours(24).AddMinutes(47), new yapılacakİşSınıfı(işlem));

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //veriÇekme.motoraşinBilgilendirme = label1;
            //veriÇekme.motoraşinVeriÇekmeyeBaşla();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //veriÇekme.dinamikBilgilendirme = label2;
            //veriÇekme.dinamikVeriÇekmeyeBaşla();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            program.Kapat();
        }


        private void button3_Click(object sender, EventArgs e)
        {
            IdeaSoftVeritabanı.brand.verileriÇek();
            //Nesne_brand.verileriÇek(label3);
            //    if (MessageBox.Show("sabitlabeller tablosu içindekiler silinip dosyadan yeniden oluşturulacak. Onaylıyor musunuz?", "Dikkat !!!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            //    {
            //        OpenFileDialog op = new OpenFileDialog();
            //        op.Filter = "Text Dosyaları|*.txt";
            //        DialogResult c = op.ShowDialog();
            //        if (c == DialogResult.Cancel) return;
            //        if (op.FileName == "") return;
            //        var v = new Thread(() => veritabanı.sabitLabelleri_DosyadanYükle(op.FileName,label2));
            //        v.IsBackground = true;
            //        v.Start();
            //    }

            //MySqlConnection con = new MySqlConnection("server=" + veritabanı.Server + ";Port=" + veritabanı.Port + ";Uid=" + veritabanı.Uid + ";Password=" + veritabanı.Pwd);
            //con.Open();
            //MySqlCommand cmd = new MySqlCommand("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = 'ttt'", con);
            //MySqlDataReader dr = cmd.ExecuteReader();
            //if (dr.Read())
            //{
            //    MessageBox.Show("~" + dr.GetValue(0).ToString() + "~");
            //}
            //else MessageBox.Show("okunamadı..");
            //dr.Close();
        }

        private void tokenBilgilendirme_TextChanged(object sender, EventArgs e)
        {
            Color renk;
            if (tokenBilgilendirme.Text.StartsWith("Durum: Aktif")) renk = Color.MediumSeaGreen;
            else if (tokenBilgilendirme.Text.StartsWith("Durum: Güncelleniyor")) renk = Color.Orange;
            else renk = Color.Red;
            toolStripStatusLabel1.BackColor = tokenBilgilendirme.ForeColor = renk;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            IdeaSoftVeritabanı.currency.verileriÇek();
            //Nesne_currency.verileriÇek(label4);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            IdeaSoftVeritabanı.price.verileriÇek();
            //Nesne_price.verileriÇek(label5);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            IdeaSoftVeritabanı.details.verileriÇek();
            //Nesne_detail.verileriÇek(label6);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            IdeaSoftVeritabanı.category.verileriÇek();
            //Nesne_category.verileriÇek(label7);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            IdeaSoftVeritabanı.product_to_category.verileriÇek();
            //Nesne_product_to_categories.verileriÇek(label8);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            IdeaSoftVeritabanı.product_image.verileriÇek();
            //Nesne_product_image.verileriÇek(label9);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            IdeaSoftVeritabanı.purchase_limitations.verileriÇek();
            //Nesne_purchase_limitations.verileriÇek(label10);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            IdeaSoftVeritabanı.purchase_limitation_items.verileriÇek();
            //Nesne_purchase_limitation_items.verileriÇek(label11);
        }

        private void button12_Click(object sender, EventArgs e)
        {
            IdeaSoftVeritabanı.product.verileriÇek();
            //Nesne_product.verileriÇek(label12);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            IdeaSoftVeritabanı.tags.verileriÇek();
            //Nesne_tags.verileriÇek(label13);
        }

        private void button14_Click(object sender, EventArgs e)
        {
            IdeaSoftVeritabanı.product_to_tags.verileriÇek();
            //Nesne_product_to_tags.verileriÇek(label14);
        }

        Thread thread_ideaSoftTümVerileriÇekme;

        void ideaSoftTümVerileriÇekme()
        {
            button3.Enabled = button4.Enabled = button5.Enabled = button6.Enabled = button7.Enabled = button8.Enabled = button9.Enabled = button10.Enabled = button11.Enabled = button12.Enabled = button13.Enabled = button14.Enabled = false;

            label3.Text = label4.Text = label5.Text = label6.Text = label7.Text = label8.Text = label9.Text = label10.Text = label11.Text = label12.Text = label13.Text = label14.Text = label15.Text = "";

            DateTime başlama = DateTime.Now;

            button3.BackColor = Color.Yellow;
            IdeaSoftVeritabanı.brand.verileriÇek();
            IdeaSoftVeritabanı.brand.T_veriÇekme.Join();
            button3.BackColor = Color.GreenYellow;

            button4.BackColor = Color.Yellow;
            IdeaSoftVeritabanı.currency.verileriÇek();
            IdeaSoftVeritabanı.currency.T_veriÇekme.Join();
            button4.BackColor = Color.GreenYellow;

            //button5.BackColor = Color.Yellow;
            //ideaSoftVeritabanı.price.verileriÇek();
            //ideaSoftVeritabanı.price.T_veriÇekme.Join();
            //button5.BackColor = Color.GreenYellow;

            button6.BackColor = Color.Yellow;
            IdeaSoftVeritabanı.details.verileriÇek();
            IdeaSoftVeritabanı.details.T_veriÇekme.Join();
            button6.BackColor = Color.GreenYellow;

            button7.BackColor = Color.Yellow;
            IdeaSoftVeritabanı.category.verileriÇek();
            IdeaSoftVeritabanı.category.T_veriÇekme.Join();
            button7.BackColor = Color.GreenYellow;

            button8.BackColor = Color.Yellow;
            IdeaSoftVeritabanı.product_to_category.verileriÇek();
            IdeaSoftVeritabanı.product_to_category.T_veriÇekme.Join();
            button8.BackColor = Color.GreenYellow;

            button9.BackColor = Color.Yellow;
            IdeaSoftVeritabanı.product_image.verileriÇek();
            IdeaSoftVeritabanı.product_image.T_veriÇekme.Join();
            button9.BackColor = Color.GreenYellow;

            button10.BackColor = Color.Yellow;
            IdeaSoftVeritabanı.purchase_limitations.verileriÇek();
            IdeaSoftVeritabanı.purchase_limitations.T_veriÇekme.Join();
            button10.BackColor = Color.GreenYellow;

            button11.BackColor = Color.Yellow;
            IdeaSoftVeritabanı.purchase_limitation_items.verileriÇek();
            IdeaSoftVeritabanı.purchase_limitation_items.T_veriÇekme.Join();
            button11.BackColor = Color.GreenYellow;

            button12.BackColor = Color.Yellow;
            IdeaSoftVeritabanı.product.verileriÇek();
            IdeaSoftVeritabanı.product.T_veriÇekme.Join();
            button12.BackColor = Color.GreenYellow;

            //button13.BackColor = Color.Yellow;
            //ideaSoftVeritabanı.tags.verileriÇek();
            //ideaSoftVeritabanı.tags.T_veriÇekme.Join();
            //button13.BackColor = Color.GreenYellow;

            //button14.BackColor = Color.Yellow;
            //ideaSoftVeritabanı.product_to_tags.verileriÇek();
            //ideaSoftVeritabanı.product_to_tags.T_veriÇekme.Join();
            //button14.BackColor = Color.GreenYellow;

            TimeSpan geçen = DateTime.Now.Subtract(başlama);
            label15.Text = "Veri çekme işlemleri bitti. İşlem süresi: " + (geçen.Days == 0 ? "" : geçen.Days.ToString() + " gün ") + (geçen.Hours == 0 ? "" : geçen.Hours.ToString() + " saat ") + (geçen.Minutes == 0 ? "" : geçen.Minutes.ToString() + " dakika ") + (geçen.Seconds == 0 ? "" : geçen.Seconds.ToString() + " saniye");

            button3.Enabled = button4.Enabled = button5.Enabled = button6.Enabled = button7.Enabled = button8.Enabled = button9.Enabled = button10.Enabled = button11.Enabled = button12.Enabled = button13.Enabled = button14.Enabled = true;

            button3.BackColor = button4.BackColor = button5.BackColor = button6.BackColor = button7.BackColor = button8.BackColor = button9.BackColor = button10.BackColor = button11.BackColor = button12.BackColor = button13.BackColor = button14.BackColor = SystemColors.Control;
        }

        private void button15_Click(object sender, EventArgs e)
        {
            if (thread_ideaSoftTümVerileriÇekme != null) if (thread_ideaSoftTümVerileriÇekme.IsAlive) return;
            thread_ideaSoftTümVerileriÇekme = new Thread(ideaSoftTümVerileriÇekme);
            thread_ideaSoftTümVerileriÇekme.Start();
        }

        private void button16_Click(object sender, EventArgs e)
        {
            //MySqlCommand cmd = new MySqlCommand();
            //cmd.Connection = new MySqlConnection(veritabanı.connectionString);
            //cmd.Connection.Open();
            //cmd.CommandText = veritabanı.IdeasoftTablosuOluşturma;
            //cmd.ExecuteNonQuery();
            //cmd.Connection.Close();
        }

        private void button17_Click(object sender, EventArgs e)
        {
            firmalar.Motoraşin.l = label1;
            firmalar.Motoraşin.verileriÇek();
        }

        private void button18_Click(object sender, EventArgs e)
        {
            firmalar.Dinamik.l = label2;
            firmalar.Dinamik.verileriÇek();
        }

        private void button19_Click(object sender, EventArgs e)
        {
            IdeaSoftVeritabanı.ideaSoft_TablosunuOluştur(label16);
        }

        private void button20_Click(object sender, EventArgs e)
        {
            log.Yaz(textBox1.Text);
        }

        private void button21_Click(object sender, EventArgs e)
        {
            Entegrasyon.datas = new RestSharp.IRestResponse[0];
            //string[] s = { "ABA KF000138", "ABA 8PK1614", "ABA 8PK1475", "ABA 7PK2060", "ABA 7PK1793", "ABA 7PK1515", "ABA 7PK1148", "ABA 6PK985" };
            MySqlConnection con = new MySqlConnection(veritabanı.connectionString);
            //MySqlConnection con2 = new MySqlConnection(veritabanı.connectionString);
            con.Open();
            //MySqlCommand cmd = new MySqlCommand("select sku from mulahaza where durum='X'", con1);
            //MySqlDataReader dr = cmd.ExecuteReader();
            //string sku = "";
            //int i = 1;
            //listBox1.Items.Clear();
            //while (i<=s.Length)
            //{
            //    sku = s[i - 1];
            //        //dr.GetString(0);
            //    listBox1.Items.Add(i.ToString() + ". " + sku + " ....");
            //    listBox1.SelectedIndex = i - 1;
            //    if (Entegrasyon.ürünSil(sku,con2)) listBox1.Items[i - 1] = i.ToString() + ". " + sku + " ..OK.."; 
            //    else listBox1.Items[i - 1] = i.ToString() + ". " + sku + " ---ERROR---";
            //    i++;
            //}
            //MessageBox.Show(Entegrasyon.datas.Length.ToString());
            ////dr.Close();

            ////object[] x = Entegrasyon.kategoriIDGetir("BİNEK 1.GRUP>DenemeÜst>Deneme>DenemeAlt", con);

            ////firmalar.Motoraşin.ortağaAktar(con);

            //firmalar.Dinamik.verilerÇekildiktenSonraYapılacaklar(con);
            //log.Yaz("Dinamik\tİşlem BİTTİ.");
            //MessageBox.Show("Dinamik İşlem Bitti. çoklu kayıtlar siliniyor.");
            //classFirma.multiRecordControlOrtak(con);
            log.Yaz("Motorasin Mulahaza");
            firmalar.Motoraşin.mulahazaEt(con);
            log.Yaz("Dinamik Mulahaza");
            firmalar.Dinamik.mulahazaEt(con);
            log.Yaz("İdeaSoft Mulahaza.");
            classFirma.mulahazaEt_ideaSoft(con);
            log.Yaz("Mulahaza BİTTİ.");
            MessageBox.Show("İşlem Bitti");
            con.Close();
        }
    }
}
