﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;
using System.Xml;
using System.IO;
using System.Globalization;
using Microsoft.VisualBasic;
using MySql.Data.MySqlClient;
using System.Security.Cryptography;
using System.Windows.Forms;
using System.Threading;
using System;
using System.Collections.Generic;

namespace NettenGelsin
{
    public static class veritabanı
    {
        public static int gönderilenPaketBoyutu = 32; //MB cinsinden

        // bu tablo isimleri değişmemeli. eğer değişirse sql fonskyon ve prosedure leri içinde de değiştirilmeleri gerekli. Bu bahsi geçen fonskyon ve prosedureler için proceduresAndFunctions değişkeni kontrol edilmeli
        public static string MotoraşinTablosu = "motorasin";
        public static string DinamikTablosu = "dinamik";
        public static string OrtakTablosu = "ortak";
        public static string SabitLabellerTablosu = "sabitLabeller";
        public static string DinamikMarkalarTablosu = "dinamikMarkalar";
        public static string DeğişimTablosu = "degisim";

        public static string Server = "127.0.0.1";
        public static string Port = "3308";
        public static string Uid = "nettengelsin";
        public static string Pwd = "netten2020gelsin";
        public static string Database = "nettenGelsin";

        public static string MotoraşinTabloYapısı = "ManufacturerCode VARCHAR(50), Name TINYTEXT, Manufacturer VARCHAR(50), Quantity VARCHAR(10), Price FLOAT, PriceCurrency VARCHAR(50), VehicleType VARCHAR(50), VehicleBrand VARCHAR(50), OrginalNo VARCHAR(50), Picture TEXT, MinOrder FLOAT, Pictures TEXT";
        public static string MotoraşinTablosuIndexYapısı = "PRIMARY KEY (`id`) USING BTREE,INDEX `ManufacturerCode` (`ManufacturerCode`) USING BTREE,INDEX `Manufacturer` (`Manufacturer`) USING BTREE,INDEX `Name` (`Name`(63)) USING BTREE,INDEX `VehicleType` (`VehicleType`) USING BTREE,INDEX `VehicleBrand` (`VehicleBrand`) USING BTREE,INDEX `Price` (`Price`) USING BTREE,INDEX `MinOrder` (`MinOrder`) USING BTREE";

        public static string DinamikTabloYapısı = "stok_kodu VARCHAR(75), stok_adi TEXT, marka VARCHAR(50), uretici_kodu VARCHAR(50), onceki_kod VARCHAR(50), kull1s VARCHAR(50), kull2s VARCHAR(50), kull3s VARCHAR(50), kull4s VARCHAR(50), kull5s VARCHAR(50), kull6s VARCHAR(50), kull7s VARCHAR(50), kull8s VARCHAR(150), resim_url TEXT, oem_liste TINYTEXT, esdegerListe TINYTEXT, fiyat FLOAT, varyok VARCHAR(5), barkod1 VARCHAR(50), barkod2 VARCHAR(50), barkod3 VARCHAR(50), kampanyaOrani INT(2), paketMiktari INT(3), koliMiktari INT(3), olcuBirimi VARCHAR(10)";
        public static string DinamikTablosuIndexYapısı = "PRIMARY KEY (`id`) USING BTREE,INDEX `stok_kodu` (`stok_kodu`) USING BTREE,INDEX `kull1s` (`kull1s`) USING BTREE,INDEX `kull7s` (`kull7s`) USING BTREE,INDEX `kull8s` (`kull8s`) USING BTREE,INDEX `marka` (`marka`) USING BTREE,INDEX `stok_adi` (`stok_adi`(63)) USING BTREE,INDEX `fiyat` (`fiyat`) USING BTREE,INDEX `varyok` (`varyok`) USING BTREE,INDEX `kampanyaOrani` (`kampanyaOrani`) USING BTREE,INDEX `paketMiktari` (`paketMiktari`) USING BTREE";

        //public static string SabitLabelTabloYapısı = "stok_kodu VARCHAR(75), label TEXT";
        //public static string SabitLabelTablosuIndexYapısı = "PRIMARY KEY (`id`) USING BTREE, INDEX `stok_kodu` (`stok_kodu`) USING BTREE";

        public static string DinamikMarkalarTabloYapısı = "marka VARCHAR(50), cekilecek BOOL DEFAULT TRUE";
        public static string DinamikMarkalarTablosuIndexYapısı = "PRIMARY KEY (`id`) USING BTREE, INDEX `marka` (`marka`) USING BTREE";

        public static string DeğişimTabloYapısı = "tabloAdi VARCHAR(30), alanAdi VARCHAR(30), eskiDeger TEXT, yeniDeger TEXT, sart VARCHAR(1) DEFAULT '='";
        public static string DeğişimTablosuIndexYapısı = "PRIMARY KEY (`id`) USING BTREE, INDEX `eskiDeger` (`eskiDeger`(85)) USING BTREE, INDEX `tabloAdi` (`tabloAdi`) USING BTREE, INDEX `alanAdi` (`alanAdi`) USING BTREE, INDEX `sart` (`sart`) USING BTREE";

        public static string OrtakTabloYapısı = "stok_kodu VARCHAR(75), label TEXT, brand VARCHAR(50), category_path TINYTEXT, barcode VARCHAR(50), price FLOAT, currency_abbr VARCHAR(10), paket_miktari INT(3), stok_amount INT(4), stock_type VARCHAR(15), discount INT(4), discountType INT(1), picture_path TEXT, nereden VARCHAR(30), durum VARCHAR(1)";
        public static string OrtakTablosuIndexYapısı = "PRIMARY KEY (`id`) USING BTREE, INDEX `stok_kodu` (`stok_kodu`) USING BTREE";

        public static string OrtakTablosuINSERT_İfadesi = "INSERT INTO " + OrtakTablosu + "(stok_kodu, label, brand, category_path, barcode, price, currency_abbr, paket_miktari, stok_amount, stock_type, discount, discountType, picture_path, nereden) ";

        //public static string IdeasoftTablosuOluşturma = "create table ideasoft (id INT(11) PRIMARY KEY, " + OrtakTabloYapısı + ") engine MyISAM select products.id, products.sku, products.name, getBrandName(products.brand_id), getPath(products.id), products.barcode, products.price1, getCurrencyAbbr(products.currency_id), getPaketMiktari(products.id), products.stockAmount, products.stockTypeLabel, getImageFileName(products.id) FROM products;";

        public static string ideasoftTablosuYapısı = "id INT(11), stok_kodu VARCHAR(75), label TEXT, brand VARCHAR(50), category_path TINYTEXT, barcode VARCHAR(50), price FLOAT, currency_abbr VARCHAR(10), paket_miktari INT(3), stok_amount INT(4), stock_type VARCHAR(15), discount INT(4), discountType INT(1), picture_path TEXT, durum VARCHAR(1)";
        public static string ideasoftTablosuIndexYapısı = "PRIMARY KEY (`id`) USING BTREE, INDEX `stok_kodu` (`stok_kodu`(75)) USING BTREE";

        public static string ideaSoftTablosuEklemeSQL = "INSERT INTO ideasoft (id, stok_kodu, label, brand, category_path, barcode, price, currency_abbr, paket_miktari, stok_amount, stock_type, discount, discountType, picture_path) SELECT products.id, products.sku, products.name, getBrandName(products.brand_id), getPath(products.id), products.barcode, products.price1, getCurrencyAbbr(products.currency_id), getPaketMiktari(products.id), products.stockAmount, products.stockTypeLabel, products.discount, products.discountType, getImageFileName(products.id) FROM products";

        public static string mulahazaTabloYapısı = "sku VARCHAR(50), durum VARCHAR(8), firma VARCHAR(30)";
        public static string mulahazaTablosuIndexYapısı = "PRIMARY KEY (`id`) USING BTREE, INDEX `sku` (`sku`) USING BTREE, INDEX `durum` (`durum`) USING BTREE, INDEX `firma` (`firma`) USING BTREE";

        public static string stokSifirSayaciTabloYapısı = "sku VARCHAR(50), kacGun INT(8)";
        public static string stokSifirSayaciTablosuIndexYapısı = "PRIMARY KEY (`id`) USING BTREE, INDEX `sku` (`sku`) USING BTREE";

        public static string slugTabloYapısı = "sku TINYTEXT, slug TEXT";
        public static string slugTablosuIndexYapısı="INDEX `sku` (`sku`(63)) USING BTREE";

        #region proceduresAndFunctions
        public static string proceduresAndFunctions = @"";

#endregion

        public static string connectionString { get { return "Server=" + Server + ";Port=" + Port + ";Uid=" + Uid + ";Pwd=" + Pwd + ";Database=" + Database; } }

        //public static void sabitLabelleri_DosyadanYükle(string dosya, Label labelBilgi)
        //{
        //    MySqlConnection con = new MySqlConnection(veritabanı.connectionString);
        //    con.Open();
        //    if (labelBilgi == null) labelBilgi = new Label();
        //    tabloOluştur_idAuto(SabitLabellerTablosu, SabitLabelTabloYapısı, SabitLabelTablosuIndexYapısı, con);
        //    FileStream f = new FileStream(dosya, FileMode.Open);
        //    StreamReader r = new StreamReader(f, Encoding.UTF8);
        //    string satır = "";
        //    string[] s;
        //    MySqlCommand cmd = new MySqlCommand();
        //    cmd.Connection = con;
        //    cmd.Parameters.Add("@sk", MySqlDbType.VarChar);
        //    cmd.Parameters.Add("@l", MySqlDbType.VarChar);
        //    cmd.CommandText = "insert into " + SabitLabellerTablosu + "(stok_kodu,label) values(@sk,@l)";
        //    int i = 0;
        //    while ((satır = r.ReadLine()) != null)
        //    {
        //        s = satır.Trim().Split('½');
        //        if (s.Length != 2) continue;
        //        if (s[0] == null || s[1] == null) continue;
        //        s[0] = s[0].Trim();
        //        s[1] = s[1].Trim();
        //        if (s[0] == "" || s[1] == "") continue;
        //        cmd.Parameters["@sk"].Value = s[0];
        //        cmd.Parameters["@l"].Value = s[1];
        //        cmd.ExecuteNonQuery();
        //        labelBilgi.Text = "İçe aktarılan kayıt sayısı: " + (++i).ToString();
        //        labelBilgi.Refresh();
        //    }
        //    r.Close();
        //    f.Close();
        //    con.Close();
        //}

        public static bool veritabanıBağlantıBilgileriniDosyadanAl(string dosyaAdresi = ".\\Database\\databaseinfo.txt")
        {
            bool dönecek = false;
            FileInfo f = new FileInfo(dosyaAdresi);
            if (f.Exists)
            {
                string a, b, c, d, e;
                FileStream file = new FileStream(f.FullName, FileMode.Open);
                StreamReader reader = new StreamReader(file, Encoding.UTF8);
                try
                {
                    a = reader.ReadLine().Split(':')[1].Trim();
                    b = reader.ReadLine().Split(':')[1].Trim();
                    c = reader.ReadLine().Split(':')[1].Trim();
                    d = reader.ReadLine().Split(':')[1].Trim();
                    e = reader.ReadLine().Split(':')[1].Trim();
                    dönecek = true;
                }
                finally
                {
                    reader.Close();
                    file.Close();
                }
                if (dönecek)
                {
                    Server = a;
                    Port = b;
                    Uid = c;
                    Pwd = d;
                    Database = e;
                }
            }
            return dönecek;
        }
        public static bool veritabanıBağlantıBilgileriniDosyayaYaz(string dosyaAdresi = ".\\Database\\databaseinfo.txt")
        {
            bool dönecek = false;
            FileInfo f = new FileInfo(dosyaAdresi);
            DirectoryInfo dizin = new DirectoryInfo(f.DirectoryName);
            if (!dizin.Exists) dizin.Create();

            FileStream file = new FileStream(f.FullName, FileMode.Create);
            StreamWriter writer = new StreamWriter(file, Encoding.UTF8);
            try
            {
                writer.WriteLine("Server:" + Server);
                writer.WriteLine("Port:" + Port);
                writer.WriteLine("Uid:" + Uid);
                writer.WriteLine("Pwd:" + Pwd);
                writer.WriteLine("Database:" + Database);
                dönecek = true;
            }
            finally
            {
                writer.Close();
                file.Close();

            }
            return dönecek;
        }

        public static short veritabanıDurumu() //0 ise veritabanı yok, 1 ise veritabanına bağlanabiliyor, -1 ise bağlantı hatası
        {
            MySqlConnection con = new MySqlConnection("server=" + Server + ";Port=" + Port + ";Uid=" + Uid + ";Password=" + Pwd);

            try
            {
                con.Open();
            }
            catch (Exception hata)
            {
                return -1;
            }
            MySqlCommand cmd = new MySqlCommand("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME= '" + Database + "'", con);
            short s = 0;
            MySqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read()) s = 1;
            else s = 0;
            dr.Close();
            con.Close();
            return s;
        }
        public static bool veritabanıOluştur()
        {
            try
            {
                MySqlConnection con = new MySqlConnection("server=" + Server + ";Port=" + Port + ";Uid=" + Uid + ";Password=" + Pwd);
                MySqlCommand cmd = new MySqlCommand("CREATE DATABASE IF NOT EXISTS " + Database, con);
                cmd.ExecuteNonQuery();
                con.Close();
                return true;
            }
            catch (Exception hata)
            {
                return false;
            }
        }
        public static bool tabloVarmı(string tabloAdı, MySqlConnection con)
        {
            bool s = true;
            try
            {
                MySqlCommand cmd = new MySqlCommand("SHOW COLUMNS FROM " + tabloAdı, con);
                MySqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read()) s = true;
                else s = false;
                dr.Close();
            }
            catch (Exception hata)
            {
                s = false;
            }
            return s;
        }
        public static bool tabloOluştur_idAuto(string tabloAdı, string alanTanımlamaları, string indexTanımlamaları, MySqlConnection con)
        {
            indexTanımlamaları = indexTanımlamaları == "" ? "" : (", " + indexTanımlamaları);
            string s = string.Format(@"
DROP TABLE if EXISTS {0}; 
create table {0} (id INT(11) UNSIGNED AUTO_INCREMENT, {1}{2}) ENGINE=MyISAM;", tabloAdı, alanTanımlamaları, indexTanımlamaları);
            MySqlCommand cmd = new MySqlCommand(s, con);
            try
            {
                cmd.CommandTimeout = 600;
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool tabloOluştur_withoutId(string tabloAdı, string alanTanımlamaları, string indexTanımlamaları, MySqlConnection con)
        {
            indexTanımlamaları = indexTanımlamaları == "" ? "" : (", " + indexTanımlamaları);
            string s = string.Format(@"
DROP TABLE if EXISTS {0}; 
create table {0} ({1}{2}) ENGINE=MyISAM;", tabloAdı, alanTanımlamaları, indexTanımlamaları);
            MySqlCommand cmd = new MySqlCommand(s, con);
            try
            {
                cmd.CommandTimeout = 600;
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        //public static bool tabloyuBoşalt(string tabloAdı, MySqlConnection con)
        //{
        //    MySqlCommand cmd = new MySqlCommand("Truncate table " + tabloAdı, con);
        //    try
        //    {
        //        cmd.ExecuteNonQuery();
        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //}
        //public static bool ortakTablosundanSil(string hangisini, MySqlConnection con)
        //{
        //    // hangisini şimdilik ya motorasin veya dinamik olabilir. istenirse başkaları da olbilir.
        //    MySqlCommand cmd = new MySqlCommand("delete from " + OrtakTablosu + " where nereden='" + hangisini + "'", con);
        //    try
        //    {
        //        cmd.CommandTimeout = 600;
        //        cmd.ExecuteNonQuery();
        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //}
        //public static bool kayıtEkle(string eklemeSorgusu, MySqlConnection con)
        //{
        //    bool x = true;
        //    MySqlCommand cmd = new MySqlCommand(eklemeSorgusu, con);
        //    try { cmd.ExecuteNonQuery(); } catch { x = false; }
        //    return x;
        //}
        public static void tabloKontrol(string tabloAdı, string tabloYapısı, string indexYapısı, MySqlConnection con)
        {
            if (!tabloVarmı(tabloAdı, con))
            {
                MessageBox.Show(tabloAdı + " tablosu olmadığı için yeni oluşturuluyor.", "Dikkat !!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                if (tabloOluştur_idAuto(tabloAdı, tabloYapısı, indexYapısı, con))
                    MessageBox.Show(tabloAdı + " tablosu oluşturuldu.");
                else
                {
                    MessageBox.Show(tabloAdı + " tablosu oluşturulamadı. Bu tablo olmadan program başlayamaz..", "Hata !", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    program.Kapat();
                }
            }
        }

        public static void fonksyonlarıOluştur(MySqlConnection con)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = con;
            cmd.CommandTimeout = 600;
            cmd.CommandText = veritabanı.proceduresAndFunctions;
            cmd.ExecuteNonQuery();
        }

        public static void Başla()
        {
            DirectoryInfo dizin = new DirectoryInfo(".\\Database");
            if (!dizin.Exists) dizin.Create();

            if (!veritabanıBağlantıBilgileriniDosyadanAl())
            {
                veritabanıBilgileri v = new veritabanıBilgileri();
                if (v.ShowDialog() == DialogResult.Cancel)
                {
                    MessageBox.Show("Veritabanı bağlantı bilgileri olmadan program başlayamaz..", "Hata !", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    program.Kapat();
                }
                else
                {
                    Server = ((int)v.numericUpDown2.Value).ToString() + "." + ((int)v.numericUpDown3.Value).ToString() + "." + ((int)v.numericUpDown4.Value).ToString() + "." + ((int)v.numericUpDown5.Value).ToString();
                    Port = ((int)v.numericUpDown1.Value).ToString();
                    Uid = v.textBox1.Text;
                    Pwd = v.textBox2.Text;
                    Database = v.textBox3.Text;
                    if (!veritabanıBağlantıBilgileriniDosyayaYaz())
                    {
                        MessageBox.Show("Veritabanı bilgileri dosyaya yazılamadı.");
                    }
                }
            }

            MySqlConnection con = new MySqlConnection(veritabanı.connectionString);

            try
            {
                con.Open();
                Thread.Sleep(10);
            }
            catch (Exception hata)
            {
                switch (veritabanıDurumu())
                {
                    case 1: MessageBox.Show(Database + " veritabanı oluşturuldu."); break;
                    case 0:
                        if (!veritabanıOluştur())
                        {
                            MessageBox.Show("Veritabanı bağlantısı sağlanamadı. Program başlatılamıyor.\nHata mesajı:\n" + hata.Message, "Hata !!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            program.Kapat();
                        }
                        else
                        {
                            con = new MySqlConnection(veritabanı.connectionString);
                        }
                        break;
                    case -1:
                        MessageBox.Show("Veritabanı bağlantısı sağlanamadı. Program başlatılamıyor. Veritabanı bağlantı bilgileri hatalı.\nHata mesajı:\n" + hata.Message, "Hata !!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        program.Kapat();
                        break;
                }
            }
            //tabloKontrol(MotoraşinTablosu, MotoraşinTabloYapısı, MotoraşinTablosuIndexYapısı, con);
            //tabloKontrol(DinamikTablosu, DinamikTabloYapısı, DinamikTablosuIndexYapısı, con);
            tabloKontrol(OrtakTablosu, OrtakTabloYapısı, OrtakTablosuIndexYapısı, con);
            //tabloKontrol(SabitLabellerTablosu, SabitLabelTabloYapısı, SabitLabelTablosuIndexYapısı, con);
            tabloKontrol(DinamikMarkalarTablosu, DinamikMarkalarTabloYapısı, DinamikMarkalarTablosuIndexYapısı, con);
            tabloKontrol(DeğişimTablosu, DeğişimTabloYapısı, DeğişimTablosuIndexYapısı, con);
            tabloKontrol("stokSifirSayaci", stokSifirSayaciTabloYapısı, stokSifirSayaciTablosuIndexYapısı, con);
            tabloKontrol("mulahaza", mulahazaTabloYapısı, mulahazaTablosuIndexYapısı, con);
            //tabloKontrol("slug", slugTabloYapısı, slugTablosuIndexYapısı, con);
            if (!tabloVarmı("slug",con))
            {
                MessageBox.Show("slug tablosu önemli bir tablo. bu tablo olmadan program başlayamaz.", "Hata !", MessageBoxButtons.OK, MessageBoxIcon.Error);
                program.Kapat();

            }
            //fonksyonlarıOluştur(con);
            //kategoriFonksonunuOluştur(con);
            con.Close();
        }
    }
}
